Little Print is located in the Melbourne CBD and provides a customer friendly environment to have business, portfolio or any other printing requirements taken care of. Our friendly staff are trained in all aspects of print and have a detailed understanding of design software to assist with anything.
 || Address: 38-40 Little La Trobe Street, Melbourne, VIC 3000
 || Phone: +61 3 9662 3677
